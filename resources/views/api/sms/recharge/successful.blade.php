Hello {{ $transaction->eCard->card_holder_name }},
Recharge of {{ $transaction->actual_amount }} has been succesfully done for FC-ECard #{{ $transaction->eCard->ecard_no }}.
@if(!empty($transaction->offer_percentage) && $transaction->offer_percentage > 0)
Congratulations! You received extra {{ number_format($transaction->transaction_amount - $transaction->actual_amount, 2) }} ({{ $transaction->offer_percentage }}%) credits with this recharge.
@endif
Credits: {{ number_format($transaction->transaction_amount, 2) }} credits
Current Balance: {{ number_format($transaction->eCard->current_balance, 2) }} credits
Date: {{ $transaction->created_date }}

Thanks,
{{ $transaction->serviceProvider->name }}
