<?php
namespace App\Http\Controllers\Api\V1\Customer;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

class DashboardController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $response = [];
            return $this->respondWithSuccess('Dashbaord data returned!', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }
}