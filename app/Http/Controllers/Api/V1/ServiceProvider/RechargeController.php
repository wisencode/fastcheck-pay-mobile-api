<?php
namespace App\Http\Controllers\Api\V1\ServiceProvider;

use App\Models\CustomerTransactionLog;
use App\Models\OnlinePaymentLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;
use Carbon\Carbon;

class RechargeController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $recharges = CustomerTransactionLog::whereServiceProviderId(auth()->id())->whereNotNull('transaction_amount')->where('transaction_type', 'RCHRG');

            if (!empty($this->request->fc_ecard_number)) {
                $recharges = $recharges->whereHas('eCard', function($q) {
                    $q->where('ecard_no', $this->request->fc_ecard_number);
                });
            }
            if (!empty($this->request->fc_ecard_holder_name)) {
                $recharges = $recharges->whereHas('eCard', function($q) {
                    $q->where('card_holder_name', 'LIKE', '%'.$this->request->fc_ecard_holder_name.'%');
                });
            }
            if (!empty($this->request->date_from)) {
                $recharges = $recharges->whereDate('created_at', '>=', date('Y-m-d', strtotime($this->request->date_from)));
            }
            if (!empty($this->request->date_to)) {
                $recharges = $recharges->whereDate('created_at', '<=', date('Y-m-d', strtotime($this->request->date_to)));
            }
            if (!empty($this->request->amount_from)) {
                $recharges = $recharges->where('transaction_amount', '>=', $this->request->amount_from);
            }
            if (!empty($this->request->amount_to)) {
                $recharges = $recharges->where('transaction_amount', '<=', $this->request->amount_to);
            }

            $recharges = $recharges->latest()->paginate(15);
            
            if(empty($recharges->items())) {
                return $this->respondWithError('No recharges found!', 200);    
            }
            
            $response = [];
            foreach($recharges->items() as $index => $recharge) {
                $response[$index]['amount'] = $recharge->transaction_amount;
                $response[$index]['description'] = $recharge->description;
                $response[$index]['status'] = "Successful";
                $response[$index]['card_holder_name'] = $recharge->eCard->card_holder_name;
                $response[$index]['ecard_no'] = $recharge->eCard->ecard_no;
                $response[$index]['created_date'] = $recharge->created_date;
                $response[$index]['created_time'] = $recharge->created_time;
            }
            
            $additionalData['pagination']['has_more_pages'] = $recharges->hasMorePages(); 
            
            return $this->respondWithSuccess('Recharges data returned!', 200, $response, $additionalData);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    public function view() {
        try {
            $rules = [
                'transaction_id' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'transaction_id.required' => 'transaction_id param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $recharge = OnlinePaymentLog::find($this->request->transaction_id);
            if (empty($recharge)) {
                return $this->respondWithError('Recharge not found', 404);
            }
            $response['recharge'] = $recharge;
            return $this->respondWithSuccess('Recharge information returned', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);   
        }
    }

}