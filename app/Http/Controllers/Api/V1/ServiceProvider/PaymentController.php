<?php

namespace App\Http\Controllers\Api\V1\ServiceProvider;

use App\Models\Payment;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Domains\Auth\Models\Customer;
use App\Events\NotifyCustomerForPaymentStatus;
use App\Models\CustomerTransactionLog;
use App\Models\PaymentVerificationCode;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Api\BaseApiController;
use App\Events\NotifyCustomerForPaymentVerification;

class PaymentController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $payments = Payment::whereServiceProviderId(auth()->user()->service_provider_id);

            if (!empty($this->request->fc_ecard_number)) {
                $payments = $payments->whereHas('eCard', function($q) {
                    $q->where('ecard_no', $this->request->fc_ecard_number);
                });
            }
            if (!empty($this->request->fc_ecard_holder_name)) {
                $payments = $payments->whereHas('eCard', function($q) {
                    $q->where('card_holder_name', 'LIKE', '%'.$this->request->fc_ecard_holder_name.'%');
                });
            }
            if (!empty($this->request->date_from)) {
                $payments = $payments->whereDate('created_at', '>=', date('Y-m-d', strtotime($this->request->date_from)));
            }
            if (!empty($this->request->date_to)) {
                $payments = $payments->whereDate('created_at', '<=', date('Y-m-d', strtotime($this->request->date_to)));
            }
            if (!empty($this->request->amount_from)) {
                $payments = $payments->where('amount', '>=', $this->request->amount_from);
            }
            if (!empty($this->request->amount_to)) {
                $payments = $payments->where('amount', '<=', $this->request->amount_to);
            }
            if (!empty($this->request->status)) {
                $payments = $payments->where('status', '=', $this->request->status);
            }

            $payments = $payments->latest()->paginate(15);
            
            if(empty($payments->items())) {
                return $this->respondWithError('No payments found!', 200);    
            }
            
            $response = [];
            foreach($payments->items() as $index => $payment) {
                $response[$index]['id'] = $payment->id;
                $response[$index]['transaction_hash'] = $payment->transactionHash;
                $response[$index]['amount'] = $payment->amount;
                $response[$index]['remarks'] = $payment->remarks_note;
                $response[$index]['status'] = ucfirst($payment->status);
                $response[$index]['card_holder_name'] = optional($payment->eCard)->card_holder_name;
                $response[$index]['ecard_no'] = optional($payment->eCard)->ecard_no;
                $response[$index]['created_date'] = $payment->created_date;
                $response[$index]['created_time'] = $payment->created_time;
            }
            
            $additionalData['pagination']['has_more_pages'] = $payments->hasMorePages(); 
            // $additionalData['pagination'] = collect(json_decode($payments->toJson()))->except(['data']); 
            
            return $this->respondWithSuccess('Payments data returned!', 200, $response, $additionalData);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    /**
     * @return json
     */
    public function view() {
        try {
            $rules = [
                'payment_id' => ['required', 'digits_between:1,30']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'payment_id.required' => 'payment_id param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $payment = Payment::find($this->request->payment_id);
            if (empty($payment)) {
                return $this->respondWithError('Payment transaction not found', 404);
            }
            return $this->respondWithSuccess('Payment information returned', 200, $payment);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);   
        }
    }

    /**
     * @return json
     */
    public function init() {
        try {
            $rules = [
                'fc_ecard_number' => ['required'],
                'amount' => ['required', 'numeric', 'min:0', 'not_in:0']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'fc_ecard_number.required' => 'FC-ECard number is required',
                'amount.required' => 'Amount must be present while making a payment',
                'amount.numeric' => 'Amount must be numeric and greater than 0 for making a payment',
                'amount.min' => 'Amount must be numeric and greater than 0 for making a payment',
                'amount.not_in' => 'Amount must be numeric and greater than 0 for making a payment'
            ]);
    
            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $service_provider_id = auth()->user()->service_provider_id;
            $customer = Customer::whereServiceProviderId($service_provider_id)->where('ecard_no', $this->request->fc_ecard_number)->first();
            
            if (empty($customer)) {
                return $this->respondWithError('FC-ECard #'.$this->request->fc_ecard_number.' does not exists in the system.', 400);
            }
            if ($customer->is_locked == 1) {
                return $this->respondWithError('FC-ECard #'.$this->request->fc_ecard_number.' is currently locked for other transaction.', 400);
            }
            if ($customer->status != 'active') {
                return $this->respondWithError('FC-ECard #'.$this->request->fc_ecard_number.' is not active any more.', 400);   
            }
            if ($customer->current_balance < $this->request->amount) {
                return $this->respondWithError('Insufficient balance in your FC-ECard #'.$this->request->fc_ecard_number.'. Current balance in this card is '.$customer->current_balance.' credits.', 400);
            }
            
            try {
                $payment = new Payment();
                $payment->transactionHash = generateHash();
                $payment->service_provider_id = auth()->user()->service_provider_id;
                $payment->amount = floatval($this->request->amount);
                $payment->fc_ecard_id = $customer->id;
                $payment->status = 'pending';
                $payment->remarks_note = $this->request->remarks ?? '';
                $payment->created_at = date('Y-m-d H:i:s');
                $payment->save();

                if ($customer->has_payment_verification) {
                    if ($payment_verification_code = $this->generateVerificationCode($payment)) {
                        if ($payment->id > 0) {
                            $customer->is_locked = 1;
                            $customer->save();
                            $response['payment'] = $payment;
                            $response['ecard_no'] = $customer->ecard_no;
                            $response['card_holder_name'] = $customer->card_holder_name;
                            $response['otp_required'] = true;
                            event(new NotifyCustomerForPaymentVerification($payment, $payment_verification_code));
                            return $this->respondWithSuccess('Payment request made to FC-ECard #'.$this->request->fc_ecard_number, 200, $response);
                        } else {
                            return $this->respondWithError('Technical Error: Please try after some time!', 400);
                        }
                    } else {
                        return $this->respondWithError('Technical Error: Please try after some time!', 400);
                    }
                } else {
                    $customer->current_balance = $customer->current_balance - $this->request->amount;
                    $customer->save();
                    $payment->status = 'approved';
                    $payment->save();
                    $response['payment'] = $payment;
                    $response['otp_required'] = false;
                    return $this->respondWithSuccess('Payment successfully completed with FC-ECard #'.$this->request->fc_ecard_number, 200, $response);
                }
            } catch (\Throwable $th) {
                return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
            }
        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }

    /**
     * @return json
     */
    public function verify() {
        try {
            $rules = [
                'otp' => ['required'],
                'payment_id' => ['required'],
                'transaction_hash' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'otp.required' => 'OTP must be provided to verify payment',
                'payment_id.required' => 'payment_id param must be provided',
                'transaction_hash.required' => 'transaction_hash param must be provided'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $payment = Payment::find($this->request->payment_id);
            if (empty($payment)) {
                return $this->respondWithError('Payment transaction not found', 404);
            }

            if ($payment->status == 'approved') {
                return $this->respondWithError('Payment already approved in the system', 400);
            } else if ($payment->status == 'cancelled') {
                return $this->respondWithError('Payment already cancelled in the system', 400);
            }

            $verifyPaymentWith = PaymentVerificationCode::whereServiceProviderId(auth()->user()->service_provider_id)->wherePaymentId($payment->id)->whereVerificationCode($this->request->otp)->first();
            if (!empty($verifyPaymentWith)) {
                $customer = Customer::find($payment->fc_ecard_id);
                if (empty($customer)) {
                    return $this->respondWithError('This card is not associated to the system', 400);
                }
                $customer_current_balance = $customer->current_balance;
                $remaing_balance = $customer_current_balance - $payment->amount;
                if ($remaing_balance > 0) {
                    try {
                        $transactionLog = new CustomerTransactionLog();
                        $transactionLog->service_provider_id = auth()->user()->service_provider_id;
                        $transactionLog->fc_ecard_id = $payment->fc_ecard_id;
                        $transactionLog->payment_id = $payment->id;
                        $transactionLog->transaction_type = 'ORDER';
                        $transactionLog->prev_balance = $customer_current_balance;
                        $transactionLog->transaction_via = 'cash';
                        $transactionLog->transaction_amount = (float) $payment->amount;
                        $transactionLog->created_at = date('Y-m-d H:i:s');
                        $transactionLog->save();

                        $customer->current_balance = (float) $remaing_balance;
                        $customer->is_locked = 0;
                        $customer->save();

                        $payment->status = 'approved';
                        $payment->save();

                        // Notify customer for approved payment transaction
                        event(new NotifyCustomerForPaymentStatus($payment));

                        // Delete payment verification codes after approval
                        PaymentVerificationCode::whereServiceProviderId(auth()->user()->service_provider_id)->wherePaymentId($this->request->payment_id)->delete();

                        $response['transaction_hash'] = $this->request->transaction_hash;
                        $response['ecard_no'] = $customer->ecard_no;
                        $response['card_holder_name'] = $customer->card_holder_name;
                        $response['current_balance'] = number_format($customer->current_balance, 2);
                        return $this->respondWithSuccess('Payment successfully verified', 200, $response);
                    } catch (\Throwable $th) {
                        return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));        
                    }
                } else {
                    return $this->respondWithError('Insufficient credits in FC-ECard #'.$customer->ecard_no, 400);       
                }
            } else {
                return $this->respondWithError('OTP is not correct for this payment transaction', 400);
            }

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }

    /**
     * @return json
     */
    public function resendOTP() {
        try {
            $rules = [
                'payment_id' => ['required'],
                'transaction_hash' => ['required']
            ];
            $validator = $this->validateParams($this->request->all(), $rules, [
                'payment_id.required' => 'payment_id param must be provided',
                'transaction_hash.required' => 'transaction_hash param must be provided'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $payment = Payment::find($this->request->payment_id);
            if (empty($payment)) {
                return $this->respondWithError('Payment transaction not found', 404);
            }

            if ($payment->status == 'approved') {
                return $this->respondWithError('Payment already approved in the system', 400);
            } else if ($payment->status == 'cancelled') {
                return $this->respondWithError('Payment already cancelled in the system', 400);
            }

            $payment_verification_code = $this->generateVerificationCode($payment);
            event(new NotifyCustomerForPaymentVerification($payment, $payment_verification_code));
            $response = [
                'id' => $payment->id,
                'transactionHash' => $payment->transactionHash,
                'amount' => $payment->amount,
                'status' => $payment->status,
                'ecard_no' => $payment->eCard->ecard_no,
                'card_holder_name' => $payment->eCard->card_holder_name,
                'created_at' => $payment->created_at,
            ];
            return $this->respondWithSuccess('Payment verification code has been sent to '.$payment->eCard->card_holder_name, 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }

    /**
     * @return json
     */
    public function cancel() {
        try {
            $rules = [
                'payment_id' => ['required'],
                'transaction_hash' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'payment_id.required' => 'payment_id param must be provided',
                'transaction_hash.required' => 'transaction_hash param must be provided'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $payment = Payment::find($this->request->payment_id);
            if (empty($payment)) {
                return $this->respondWithError('Payment transaction not found', 404);
            }

            if ($payment->status == 'approved') {
                return $this->respondWithError('Payment already approved in the system', 400);
            } else if ($payment->status == 'cancelled') {
                return $this->respondWithError('Payment already cancelled in the system', 400);
            }

            $FCECard = Customer::find($payment->fc_ecard_id);
            $FCECard->is_locked = 0;
            $FCECard->save();

            $payment->status = 'cancelled';
            $payment->save();

            // Notify customer for cancelled payment transaction
            event(new NotifyCustomerForPaymentStatus($payment));

            $response = $payment;
            return $this->respondWithSuccess('Payment request is successfully cancelled', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }

    /**
     * @return json
     */
    public function receipt() {
        try {
            $rules = [
                'payment_id' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'payment_id.required' => 'payment_id param must be provided',
                'transaction_hash.required' => 'transaction_hash param must be provided'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $payment = Payment::find($this->request->payment_id);
            if (empty($payment)) {
                return $this->respondWithError('Payment transaction not found', 404);
            }

            $response = [
                'payment_id' => $payment->id,
                'receipt_url' => $payment->receiptURL
            ];
            return $this->respondWithSuccess('Payment receipt generated successfully', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }

    /**
     * @return string
     */
    public function generateVerificationCode($payment) {
        $paymentVerificationCode = new PaymentVerificationCode();
        $paymentVerificationCode->payment_id = $payment->id;
        $paymentVerificationCode->service_provider_id = $payment->service_provider_id;
        $paymentVerificationCode->verification_code = generateOTP(6);
        $paymentVerificationCode->save();
        return $paymentVerificationCode->verification_code;
    }

}