<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentInitiated extends Mailable
{
    use Queueable, SerializesModels;

    public $transaction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                ->subject('Payment request received for FC-ECard #'.$this->transaction->payment->eCard->ecard_no)
                ->markdown('api.mail.payment.initiated', [
                    'payLink' => $this->transaction->payLink,
                    'payment' => $this->transaction->payment,
                    'payment_verification_code' => $this->transaction->payment_verification_code
                ]);
    }
}
