<?php
namespace App\Http\Controllers\Api\V1\ServiceProvider;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;
use App\Models\Payment;
use App\Models\OnlinePaymentLog;
use App\Models\RechargeOffer;
use App\Models\CustomerTransactionLog;
use Carbon\Carbon;

class DashboardController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $response = [
                'transactions' => [],
                'recharges' => [],
                'recharge_offers' => []
            ];
            
            $service_provider_id = auth()->user()->service_provider_id;
            $latestTransactions = Payment::whereServiceProviderId($service_provider_id)->latest()->limit(5)->get();

            if(! $latestTransactions->isEmpty()) {
                foreach($latestTransactions as $index => $transaction) {
                    $response['transactions'][$index]['id'] = $transaction->id;
                    $response['transactions'][$index]['transaction_hash'] = $transaction->transactionHash;
                    $response['transactions'][$index]['remarks'] = $transaction->remarks_note;
                    $response['transactions'][$index]['amount'] = $transaction->amount;
                    $response['transactions'][$index]['status'] = ucfirst($transaction->status);
                    $response['transactions'][$index]['card_holder_name'] = $transaction->eCard->card_holder_name;
                    $response['transactions'][$index]['ecard_no'] = optional($transaction->eCard)->ecard_no;
                    $response['transactions'][$index]['created_date'] = $transaction->created_date;
                    $response['transactions'][$index]['created_time'] = $transaction->created_time;
                }
            }

            $latestRecharges = CustomerTransactionLog::whereServiceProviderId($service_provider_id)->whereNotNull('transaction_amount')->where('transaction_type', 'RCHRG')->latest()->limit(5)->get();

            if(! $latestRecharges->isEmpty()) {
                foreach($latestRecharges as $index => $recharge) {
                    $response['recharges'][$index]['order_id'] = uniqid();
                    $response['recharges'][$index]['recharge_offer'] = [
                        'eligible_amount' => 1500,
                        'offer_percentage' => 18,
                        'description' => '18% exclusive flat extra credits on 1500 recharge. Get it now before it expires!',
                        'status' => 'Active'
                    ];
                    $response['recharges'][$index]['amount'] = $recharge->transaction_amount;
                    $response['recharges'][$index]['description'] = $recharge->description;
                    $response['recharges'][$index]['pay_with'] =  $recharge->transaction_via;
                    $response['recharges'][$index]['status'] =  'Successful';
                    $response['recharges'][$index]['card_holder_name'] = optional($recharge->eCard)->card_holder_name;
                    $response['recharges'][$index]['ecard_no'] = optional($recharge->eCard)->ecard_no;
                    $response['recharges'][$index]['created_date'] = $recharge->created_date;
                    $response['recharges'][$index]['created_time'] = $recharge->created_time;
                }
            }

            $latestRechargeOffers = RechargeOffer::whereServiceProviderId($service_provider_id)->whereStatus(1)->whereNotNull('eligible_amount')->latest()->limit(5)->get();

            if(! $latestRechargeOffers->isEmpty()) {
                foreach($latestRechargeOffers as $index => $rechargeOffer) {
                    $response['recharge_offers'][$index]['eligible_amount'] = $rechargeOffer->eligible_amount;
                    $response['recharge_offers'][$index]['offer_percentage'] = $rechargeOffer->offer_percentage;
                    $response['recharge_offers'][$index]['description'] = $rechargeOffer->description;
                    $response['recharge_offers'][$index]['status'] = $rechargeOffer->statusMode();
                }
            }

            $todayData = [
                'amount' => Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereDate('created_at', Carbon::now())->sum('amount'),
                'no_of_transactions' => Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereDate('created_at', Carbon::now())->count()
            ];
            $weekData = [
                'amount' => Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('amount'),
                'no_of_transactions' => Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count()
            ];
            $monthData = [
                'amount' => Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereMonth('created_at', Carbon::now())->sum('amount'),
                'no_of_transactions' => Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereMonth('created_at', Carbon::now())->count()
            ];

            $stats = [
                'today' => (int) Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereDate('created_at', Carbon::now())->sum('amount'),
                'this_week' => (int) Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('amount'),
                'this_month' => (int) Payment::whereServiceProviderId($service_provider_id)->whereStatus('approved')->whereMonth('created_at', Carbon::now())->sum('amount'),
                'at_today' => $todayData,
                'at_this_week' => $weekData,
                'at_this_month' => $monthData
            ];
            $response['stats'] = $stats;

            return $this->respondWithSuccess('Dashbaord data returned!', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }
}