<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Payment;
use Illuminate\Support\Str;
use App\Domains\Auth\Models\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerTransactionLog.
 */
class CustomerTransactionLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fc_ecard_id',
        'service_provider_id',
        'transaction_type',
        'payment_id',
        'transaction_amount',
        'actual_amount',
        'offer_percentage',
        'transaction_via',
        'prev_balance',
        'created_at'
    ];

    protected $table = 'fc_ecards_transactions_log';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'transaction_amount' => 'float',
        'created_at' => 'datetime:Y-m-d H:i:s'
    ];

    public $timestamps = false;

    protected $appends = ['mini_description'];

    public function getCreatedDateAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d');
    }
    public function getCreatedTimeAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');
    }

    public function eCard()
    {
        return $this->belongsTo(Customer::class, 'fc_ecard_id');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public function serviceProvider() {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }

    public function getDescriptionAttribute() {
        $logType = $this->transaction_type;
        $description = '';
        if ($logType == 'INTRCHRG') {
            $description = 'Initial recharge of '.$this->actual_amount.$this->serviceProvider->currency->symbol;
            $description.= ' [Offer - '.$this->offer_percentage.'%]';
        } else if ($logType == 'RCHRG') {
            $description = 'Recharge of '.$this->actual_amount.$this->serviceProvider->currency->symbol;
            if ($this->offer_percentage > 0) {
                $description.= ' [Offer - '.$this->offer_percentage.'%]';
            }
        } else if ($logType == 'ORDER') {
            $description = 'Transaction of '.$this->transaction_amount.$this->serviceProvider->currency->symbol;
        }
        return $description;
    }

    public function getMiniDescriptionAttribute() {
        $logType = $this->transaction_type;
        $description = '';
        if ($logType == 'RCHRG') {
            if ($this->offer_percentage > 0) {
                $description = $this->offer_percentage.'% extra credits on '.$this->actual_amount;
            }
        } else if ($logType == 'ORDER') {
            if (!empty($this->payment->remarks_note)) {
                $description = Str::limit($this->payment->remarks_note, 25, '...');
            }
        }
        return $description;
    }

}
