@component('mail::message')
# Hello {{ $payment->eCard->card_holder_name }},
 
Payment request of {{ $payment->amount }} credits has been received for FC-ECard <strong>#{{ $payment->eCard->ecard_no }}</strong>. OTP for this transaction is <strong>{{ $payment_verification_code }}</strong>
 
@component('mail::button', ['url' => $payLink])
Verify Payment
@endcomponent

Thanks,<br>
{{ $payment->serviceProvider->name }}
@endcomponent