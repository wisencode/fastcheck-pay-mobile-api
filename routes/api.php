<?php

use App\Http\Controllers\Api\V1\Customer\AuthController;
use App\Http\Controllers\Api\V1\Customer\DashboardController;
use App\Http\Controllers\Api\V1\ServiceProvider\AuthController as ServiceProviderAuthController;
use App\Http\Controllers\Api\V1\ServiceProvider\DashboardController as ServiceProviderDashboardController;
use App\Http\Controllers\Api\V1\ServiceProvider\PaymentController;
use App\Http\Controllers\Api\V1\ServiceProvider\RechargeController;
use App\Http\Controllers\Api\V1\ServiceProvider\RechargeOfferController;
use App\Http\Controllers\Api\V1\ServiceProvider\ProfileController;
use App\Http\Controllers\Api\V1\ServiceProvider\FCECardController;
use App\Http\Controllers\Api\V1\ServiceProvider\CurrencyController;
use App\Http\Controllers\Api\V1\AppOwner\InfoController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'namespace' => 'Api\\V1',
    'prefix' => 'v1',
], function(){

    // App Owner Related API routes
    // Customer Related API routes
    Route::group([
        'namespace' => 'AppOwner',
        'prefix' => 'owner',
    ], function(){
        Route::group([], function() {
            Route::get('contact-details', [InfoController::class, 'contact']);
            Route::post('/card/info', [FCECardController::class, 'view']);
        });
    });

    // Customer Related API routes
    Route::group([
        'namespace' => 'Customer',
        'prefix' => 'customer',
    ], function(){
        // Authentication related routes
        Route::group([
            'prefix' => 'auth'
        ], function(){
            Route::post('login', [AuthController::class, 'login']);
            Route::post('verify-otp', [AuthController::class, 'verifyOtp']);
            Route::post('resend-otp', [AuthController::class, 'resendOTP']);
        });
        
        /**
         * These routes needs api token
         * And token must be of customer
         */
        Route::group([
            'middleware' => 'auth:customers'
        ], function(){
            Route::post('auth/logout', [AuthController::class, 'logout']);

            Route::get('dashboard', [ServiceProviderDashboardController::class, 'index']);
        });
    });
    
    // Service Provider Related API routes
    Route::group([
        'namespace' => 'ServiceProvider',
        'prefix' => 'service-provider',
    ], function(){
        // Authentication related routes
        Route::group([
            'prefix' => 'auth'
        ], function(){
            Route::post('login', [ServiceProviderAuthController::class, 'login']);
            Route::post('verify-otp', [ServiceProviderAuthController::class, 'verifyOtp']);
            Route::post('resend-otp', [ServiceProviderAuthController::class, 'resendOTP']);
        });
        
        /**
         * These routes needs api token
         * And token must be of service provider
         */
        Route::group([
            'middleware' => 'auth:service-providers'
        ], function(){
            Route::post('auth/logout', [ServiceProviderAuthController::class, 'logout']);

            Route::get('dashboard', [ServiceProviderDashboardController::class, 'index']);
            Route::get('profile', [ProfileController::class, 'index']);
            Route::patch('profile', [ProfileController::class, 'update']);
            Route::post('update-logo', [ProfileController::class, 'updateLogo']);
            Route::get('quick-pay-code', [ProfileController::class, 'qr']);

            // Payments related routes
            Route::group([
                'prefix' => 'payments'
            ], function(){
                Route::get('/', [PaymentController::class, 'index']);
                Route::get('/info', [PaymentController::class, 'view']);
                Route::post('/init', [PaymentController::class, 'init']);
                Route::post('/resend-otp', [PaymentController::class, 'resendOTP']);
                Route::post('/verify', [PaymentController::class, 'verify']);
                Route::post('/cancel', [PaymentController::class, 'cancel']);
                Route::get('/receipt', [PaymentController::class, 'receipt']);
            });

            // Recharges related routes
            Route::group([
                'prefix' => 'recharges'
            ], function(){
                Route::get('/', [RechargeController::class, 'index']);
                Route::get('/info', [RechargeController::class, 'view']);
            });

            // Recharge offers related routes
            Route::group([
                'prefix' => 'recharge-offers'
            ], function(){
                Route::get('/', [RechargeOfferController::class, 'index']);
                Route::get('/info', [RechargeOfferController::class, 'view']);
                Route::post('/create', [RechargeOfferController::class, 'create']);
                Route::delete('/delete', [RechargeOfferController::class, 'delete']);
            });

            // FC-ECard related routes
            Route::group([
                'prefix' => 'cards'
            ], function(){
                Route::get('/', [FCECardController::class, 'index']);
                Route::get('/info', [FCECardController::class, 'view']);
                Route::post('/share-to-customer', [FCECardController::class, 'shareToCustomer']);
                Route::post('/download-copy', [FCECardController::class, 'downloadCopy']);
                Route::post('/setting', [FCECardController::class, 'setting']);
                Route::post('/create', [FCECardController::class, 'create']);
                Route::post('/recharge', [FCECardController::class, 'recharge']);
            });

            // Currency routes
            Route::group([
                'prefix' => 'currency'
            ], function(){
                Route::get('/', [CurrencyController::class, 'index']);
            });            

        });
    });
});