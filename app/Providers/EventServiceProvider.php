<?php

namespace App\Providers;

use App\Domains\Auth\Listeners\RoleEventListener;
use App\Domains\Auth\Listeners\UserEventListener;
use App\Events\NotifyCustomerForPaymentStatus;
use App\Events\NotifyCustomerForPaymentVerification;
use App\Events\NotifyCustomerForRecharge;
use App\Events\NotifyCustomersForRechargeOffer;
use App\Listeners\CustomerNotifiedForPaymentStatus;
use App\Listeners\CustomerNotifiedForPaymentVerification;
use App\Listeners\CustomerNotifiedForRecharge;
use App\Listeners\CustomerNotifiedForRechargeOffer;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider.
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NotifyCustomerForPaymentVerification::class => [
            CustomerNotifiedForPaymentVerification::class,
        ],
        NotifyCustomerForPaymentStatus::class => [
            CustomerNotifiedForPaymentStatus::class,
        ],
        NotifyCustomerForRecharge::class => [
            CustomerNotifiedForRecharge::class,
        ],
        NotifyCustomersForRechargeOffer::class => [
            CustomerNotifiedForRechargeOffer::class,
        ],
    ];

    /**
     * Class event subscribers.
     *
     * @var array
     */
    protected $subscribe = [
        RoleEventListener::class,
        UserEventListener::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
