<?php

namespace App\Domains\Auth\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VerificationCode.
 */
class VerificationCode extends Model
{
    protected $table = 'fc_ecard_holder_login_verification_codes';
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fc_ecard_id',
        'verification_code',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at'
    ];

    public function setCreatedAtAttribute() {
        return $this->attributes['created_at'] = carbon('now');
    }

    public function fcCard() {
        return $this->belongsTo(Customer::class, 'fc_ecard_id', 'id');
    }
}
