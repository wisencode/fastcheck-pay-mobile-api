<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Barryvdh\DomPDF\Facade\Pdf;

class SendFCCardCopy extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $FCECard;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($FCECard, $data)
    {
        $this->FCECard = $FCECard;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $pdf = PDF::loadView('api.fc-ecard.view', $this->data);
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Digital copy for FC ECard #'.$this->FCECard->ecard_no)
            ->markdown('api.fc-ecard.view-raw', $this->data)
            ->attachData($pdf->output(), $this->FCECard->ecard_no.".pdf");
    }
}
