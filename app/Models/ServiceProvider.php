<?php

namespace App\Models;

use App\Domains\Auth\Models\Customer;
use App\Models\Currency;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class ServiceProvider.
 */
class ServiceProvider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'about_us',
        'logo',
        'currency_id',
        'address',
        'contact_number',
        'gst_number',
        'tag_line',
        'status',
        'has_online_payment_support'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s'
    ];

    protected $with = ['currency'];

    protected $appends = ['logo_url'];

    protected $hidden = ['credentials'];

    public function getLogoUrlAttribute() {
        if (!empty($this->logo)) {
            return config('app.fcpay_root_asset_url').'img/uploads/'.$this->id.'/logo/'.$this->logo;
        }
        return "https://www.gravatar.com/avatar/".md5(strtolower(trim($this->name))) . "?d=mp";
    }

    public function getMyQuickPayCodeAttribute() {
        return config('app.fcpay_root_asset_url').'img/uploads/'.$this->id.'/quick-pay-code/service-provider-qpc.'.config('app.quick_pay_qr_format');
    }

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function customers(array $only = []) {
        if (!empty($only)) {
            return $this->hasMany(Customer::class, 'service_provider_id')->get()->pluck(implode(',', $only));
        }
        return $this->hasMany(Customer::class, 'service_provider_id');
    }

}
