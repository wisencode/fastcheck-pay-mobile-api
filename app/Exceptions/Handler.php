<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Handler.
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        GeneralException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if($request->wantsJson() || $request->is('api/*')){
            if($exception instanceof ModelNotFoundException) {
                $model = explode('\\', $exception->getMessage());
                return response()->json(['success' => 0, 'message' => str_replace(']', '', end($model)). ' not found!', 'error_code' => 404], 404);
            }
            
            if($exception instanceof AuthenticationException) {
                return response()->json(['success' => 0, 'message' => $exception->getMessage(), 'error_code' => 401], 401);
            }
            
            if($exception instanceof NotFoundHttpException) {
                return response()->json(['success' => 0, 'message' => 'Api endpoint not found!', 'error_code' => 404], 404);
            }
            
            if($exception instanceof ValidationException) {
                return response()->json(['success' => 0, 'message' => collect($exception->errors())->first(), 'error_code' => 400], 400);
            }

            return response()->json(['success' => 0, 'message' => $exception->getMessage(), 'error_code' => 500], 500);
        }

        if ($exception instanceof UnauthorizedException) {
            return redirect()
                ->route(homeRoute())
                ->withFlashDanger(__('You do not have access to do that.'));
        }

        if ($exception instanceof AuthorizationException) {
            return redirect()
                ->back()
                ->withFlashDanger($exception->getMessage() ?? __('You do not have access to do that.'));
        }

        if ($exception instanceof ModelNotFoundException) {
            return redirect()
                ->route(homeRoute())
                ->withFlashDanger(__('The requested resource was not found.'));
        }

        return parent::render($request, $exception);
    }
}
