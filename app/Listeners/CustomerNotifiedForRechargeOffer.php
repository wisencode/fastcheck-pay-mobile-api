<?php

namespace App\Listeners;

use App\Mail\RechargeOfferAdded;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerNotifiedForRechargeOffer
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $rechargeOffer = $event->rechargeOffer;
        $recipients = array_unique($rechargeOffer->serviceProvider->customers(['linked_email'])->toArray(), SORT_STRING);
        // Mail::to($recipients)->send(new RechargeOfferAdded($rechargeOffer));
    }
}
