<?php
namespace App\Http\Controllers\Api\V1\ServiceProvider;

use App\Mail\SendFCCardCopy;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Mail;
use App\Domains\Auth\Models\Customer;
use App\Models\CustomerTransactionLog;
use App\Events\NotifyCustomerForRecharge;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Http\Controllers\Api\BaseApiController;

class FCECardController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $FCECards = Customer::whereServiceProviderId(auth()->user()->service_provider_id);
            
            if (!empty($this->request->fc_ecard_number)) {
                $FCECards = $FCECards->where('ecard_no', '=', $this->request->fc_ecard_number);
            }
            if (!empty($this->request->card_holder_name)) {
                $FCECards = $FCECards->where('card_holder_name', 'LIKE', $this->request->card_holder_name.'%');
            }
            if (!empty($this->request->linked_phone_number)) {
                $FCECards = $FCECards->where('linked_phone_number', 'LIKE', '%'.$this->request->linked_phone_number);
            }
            if (!empty($this->request->linked_email)) {
                $FCECards = $FCECards->where('linked_email', '=', $this->request->linked_email);
            }
            if (!empty($this->request->date_from)) {
                $FCECards = $FCECards->whereDate('created_at', '>=', date('Y-m-d', strtotime($this->request->date_from)));
            }
            if (!empty($this->request->date_to)) {
                $FCECards = $FCECards->whereDate('created_at', '<=', date('Y-m-d', strtotime($this->request->date_to)));
            }

            $FCECards = $FCECards->latest()->paginate(15);

            if(empty($FCECards->items())) {
                return $this->respondWithError('No cards found!', 200);
            }
            
            $response = [];
            foreach($FCECards->items() as $index => $card) {
                $response[$index]['id'] = $card->id;
                $response[$index]['ecard_no'] = $card->ecard_no;
                $response[$index]['current_balance'] = $card->current_balance;
                $response[$index]['name_on_card'] = $card->display_name;
                $response[$index]['card_holder_name'] = $card->card_holder_name;
                $response[$index]['linked_phone_number'] = $card->linked_phone_number;
                $response[$index]['linked_email'] = $card->linked_email;
                $response[$index]['status'] = ucfirst($card->status);
                $response[$index]['inititals'] = $card->name_initials;
                $response[$index]['avatar_bg'] = getRandomColor($index);
                $response[$index]['is_locked'] = $card->is_locked;
                $response[$index]['has_payment_verification'] = $card->has_payment_verification;
                $response[$index]['created_date'] = $card->created_date;
                $response[$index]['created_time'] = $card->created_time;
            }
            
            $additionalData['pagination']['has_more_pages'] = $FCECards->hasMorePages(); 
            
            return $this->respondWithSuccess('FC-ECards data data returned!', 200, $response, $additionalData);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    /**
     * @return Customer
     */
    protected function get($param) {
        $FCECard = [];
        if (is_numeric($param))
            $FCECard = Customer::find($param);
        if (empty($FCECard)) 
            if (!empty($this->request->from_owner) && $this->request->from_owner) {
                $FCECard = Customer::where('ecard_no', $param)->first();
            } else {
                $FCECard = Customer::whereServiceProviderId(auth()->user()->service_provider_id)->where('ecard_no', $param)->first();
            }
        return $FCECard;
    }

    /**
     * @return json
     */
    public function view() {
        try {
            $rules = [
                'identifier' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'identifier.required' => 'identifier param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $FCECard = $this->get($this->request->identifier);
            
            if (empty($FCECard)) {
                return $this->respondWithError('Card not found in the system', 404);
            }

            $response = [
                'id' => $FCECard->id,
                'ecard_no' => $FCECard->ecard_no,
                'current_balance' => $FCECard->current_balance,
                'name_on_card' => $FCECard->display_name,
                'card_holder_name' => $FCECard->card_holder_name,
                'linked_phone_number' => $FCECard->linked_phone_number,
                'linked_email' => $FCECard->linked_email,
                'status' => ucfirst($FCECard->status),
                'is_locked' => $FCECard->is_locked,
                'has_payment_verification' => $FCECard->has_payment_verification,
                'created_date' => $FCECard->created_date,
                'created_time' => $FCECard->created_time,
                'transactions' => $FCECard->transactionsLog,
            ];
            if (!empty($this->request->from_owner) && $this->request->from_owner) {
                $response['service_provider'] = [
                    'id' => $FCECard->serviceProvider->id,
                    'name' => $FCECard->serviceProvider->name,
                    'address' => $FCECard->serviceProvider->address,
                    'logo' => $FCECard->serviceProvider->logo
                ];
            }
            return $this->respondWithSuccess('Card information returned', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);   
        }
    }

    /**
     * @return json
     */
    public function setting() {
        try {
            $rules = [
                'identifier' => ['required'],
                'key' => ['required'],
                'value' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'identifier.required' => 'identifier param is required',
                'key.required' => 'key param is required',
                'value.required' => 'value param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $FCECard = $this->get($this->request->identifier);
            if (empty($FCECard)) { 
                return $this->respondWithError('Card not found in the system', 404);
            }

            $key = $this->request->key;
            $value = $this->request->value;

            switch ($key) {
                case 'status':
                    if (in_array($value, ['active', 'inactive', 'expired'])) {
                        $FCECard->status = $value;
                        $FCECard->save();
                    } else {
                        return $this->respondWithError($value. ' is not a valid value for '.$key.' setting', 400);
                    }
                    break;
                case 'is_locked':
                    if (is_numeric($value) && in_array($value, [0, 1])) {
                        $FCECard->is_locked = intval($value);
                        $FCECard->save();
                    } else {
                        return $this->respondWithError($value. ' is not a valid value for '.$key.' setting', 400);
                    }
                    break;
                case 'has_payment_verification':
                    if (is_numeric($value) && in_array($value, [0, 1])) {
                        $FCECard->has_payment_verification = intval($value);
                        $FCECard->save();
                    } else {
                        return $this->respondWithError($value. ' is not a valid value for '.$key.' setting', 400);
                    }
                    break;
                default:
                    return $this->respondWithError($key. ' is not a valid setting', 400);
                    break;
            }

            $response['fc_ecard'] = $FCECard;
            return $this->respondWithSuccess('Card setting saved', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);   
        }
    }

    /**
     * @return json
     */
    public function create() {
        try {
            $rules = [
                'card_holder_name' => ['required', 'max:80'],
                'name_on_card' => ['required', 'max:8'],
                'phone_number' => ['sometimes', 'nullable', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
                'email' => ['sometimes', 'nullable', 'email'],
                'initial_recharge' => ['sometimes', 'numeric'],
                'initial_recharge_offer_percentage' => ['sometimes', 'numeric', 'between:0,100'],
                'has_payment_verification' => ['required', 'integer', 'gte:0', 'lt:2']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'identifier.required' => 'identifier param is required',
                'key.required' => 'key param is required',
                'value.required' => 'value param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            if (empty($this->request->phone_number) && empty($this->request->email)) {
                return $this->respondWithError('At least one field from phone number and email must be filled', 400);
            }

            $initial_recharge = $this->request->initial_recharge ?? 0;
            $initial_recharge_offer_perc = $this->request->initial_recharge_offer_percentage ?? 0;

            if ($initial_recharge > 0) {
                $initial_recharge = $initial_recharge + ($initial_recharge * ($initial_recharge_offer_perc / 100));
            } else {
                return $this->respondWithError('Recharge amount must be greater than 0', 400);
            }

            $FCECard = new Customer();
            $FCECard->service_provider_id = auth()->user()->service_provider_id;
            $FCECard->card_holder_name = $this->request->card_holder_name;
            $FCECard->display_name = strtoupper(substr($this->request->name_on_card, 0, 8));
            $FCECard->linked_phone_number = $this->request->phone_number;
            $FCECard->linked_email = $this->request->email;
            $FCECard->card_holder_name = $this->request->card_holder_name;
            $FCECard->has_payment_verification = $this->request->has_payment_verification;
            $FCECard->status = 'active';
            $FCECard->valid_for = 3;
            $FCECard->current_balance = $initial_recharge;
            $FCECard->save();

            try {
                $FCECard->ecard_no = generateFCECardNumber($FCECard->id, $FCECard->service_provider_id);
                $FCECard->save();
            } catch (\Throwable $th) {
                $FCECard->delete();
                return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
            }

            try {
                $transactionLog = new CustomerTransactionLog();
                $transactionLog->service_provider_id = auth()->user()->service_provider_id;
                $transactionLog->fc_ecard_id = $FCECard->id;
                $transactionLog->transaction_type = 'INTRCHRG';
                $transactionLog->prev_balance = 0;
                $transactionLog->transaction_via = 'cash';
                $transactionLog->transaction_amount = (float) $initial_recharge;
                $transactionLog->actual_amount = $this->request->initial_recharge;
                $transactionLog->offer_percentage = $this->request->initial_recharge_offer_percentage;
                $transactionLog->created_at = date('Y-m-d H:i:s');
                $transactionLog->save();

                // Send FC ECard digital copy to customer email if email provided
                if (!empty($FCECard->linked_email)) {
                    $data = [
                        'card' => $FCECard,
                        'cardQR' => base64_encode($this->getQR($FCECard))
                    ];
                    Mail::to($FCECard->linked_email)->send(new SendFCCardCopy($FCECard, $data));
                }

            } catch (\Throwable $th) {
                $FCECard->delete();
                return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
            }

            $response['fc_ecard'] = $FCECard;
            return $this->respondWithSuccess('Card #'.$FCECard->ecard_no.' successfully generated.', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);   
        }
    }

    /**
     * @return json
     */
    public function recharge() {
        try {
            $rules = [
                'identifier' => ['required'],
                'recharge_amount' => ['required', 'numeric', 'gt:0'],
                'recharge_offer_percentage' => ['sometimes', 'numeric', 'gte:0', 'lte:100']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'identifier.required' => 'identifier param is required',
                'recharge_amount.required' => 'Recharge amount param is required',
                'recharge_offer_percentage.required' => 'Offer percentage param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $FCECard = $this->get($this->request->identifier);
            if (empty($FCECard)) { 
                return $this->respondWithError('Card not found in the system', 404);
            }

            $recharge_amount = $this->request->recharge_amount ?? 0;
            $recharge_offer_percentage = $this->request->recharge_offer_percentage ?? 0;

            if ($recharge_amount > 0) {
                $recharge_amount = $recharge_amount + ($recharge_amount * ($recharge_offer_percentage / 100));
            }

            if ($recharge_amount > 0) {
                try {
                    $transactionLog = new CustomerTransactionLog();
                    $transactionLog->service_provider_id = auth()->user()->service_provider_id;
                    $transactionLog->fc_ecard_id = $FCECard->id;
                    $transactionLog->transaction_type = 'RCHRG';
                    $transactionLog->prev_balance = $FCECard->current_balance;
                    $transactionLog->transaction_via = 'cash';
                    $transactionLog->transaction_amount = (float) $recharge_amount;
                    $transactionLog->actual_amount = (float) $this->request->recharge_amount;
                    $transactionLog->offer_percentage = (float) $this->request->recharge_offer_percentage;
                    $transactionLog->created_at = date('Y-m-d H:i:s');
                    $transactionLog->save();
                } catch (\Throwable $th) {
                    return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
                }

                try {
                    $FCECard->current_balance += $recharge_amount;
                    $FCECard->save();

                    // Notify customer for recharge transaction
                    event(new NotifyCustomerForRecharge($transactionLog));

                    return $this->respondWithSuccess($recharge_amount.' credits have been added to card #'.$FCECard->ecard_no, 200, $FCECard);
                } catch (\Throwable $th) {
                    return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
                }

            } else {
                return $this->respondWithError('Recharge amount must be greater than 0', 400);
            }

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    /**
     * @return json
     */
    public function shareToCustomer() {
        try {
            $rules = [
                'identifier' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'identifier.required' => 'identifier param is required'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $FCECard = $this->get($this->request->identifier);
            if (empty($FCECard)) { 
                return $this->respondWithError('Card not found in the system', 404);
            }

            $data = [
                'card' => $FCECard,
                'cardQR' => base64_encode($this->getQR($FCECard))
            ];

            // Send FC ECard digital copy to customer email
            if (!empty($FCECard->linked_email)) {
                Mail::to($FCECard->linked_email)->send(new SendFCCardCopy($FCECard, $data));
                return $this->respondWithSuccess('Card '.$FCECard->ecard_no.' successfully shared with '.$FCECard->card_holder_name, 200);
            }
            return $this->respondWithSuccess('Card '.$FCECard->ecard_no.' has no any linked email address', 200);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    /**
     * @return json
     */
    public function downloadCopy() {
        try {
            $rules = [
                'identifier' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'identifier.required' => 'identifier param is required'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $FCECard = $this->get($this->request->identifier);
            if (empty($FCECard)) { 
                return $this->respondWithError('Card not found in the system', 404);
            }

            // Get card copy url
            $pdf = PDF::loadView('api.fc-ecard.view', [
                'card' => $FCECard,
                'cardQR' => base64_encode($this->getQR($FCECard))
            ]);
            
            $saveFCECardCopyTo = 'fc-ecards/'.$FCECard->id.'/';
            if (!\Storage::disk('public')->exists($saveFCECardCopyTo)) {
                \Storage::disk('public')->makeDirectory($saveFCECardCopyTo);
            }
            $saveFCECardCopyAs = $FCECard->ecard_no.'.pdf';
            \Storage::disk('public')->put($saveFCECardCopyTo.$saveFCECardCopyAs, $pdf->output(), 'public');
            $response = [
                'fc_ecard_copy_url' => \Storage::disk('public')->url($saveFCECardCopyTo.$saveFCECardCopyAs)
            ];
            
            return $this->respondWithSuccess('Card '.$FCECard->ecard_no.' successfully downloaded', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    public function getQR($FCECard) {
        $cardQR = QrCode::size(150)
                ->format(env('QUICK_PAY_QR_FORMAT', 'svg'))
                ->color(255, 255, 255)
                ->backGroundColor(36, 40, 48)
                ->errorCorrection('H')
                ->style('round')
                ->generate($FCECard->ecard_no);
        return $cardQR;
    }

}