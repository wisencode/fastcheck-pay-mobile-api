<?php

namespace App\Domains\Auth\Models;

use Illuminate\Foundation\Auth\User;

/**
 * Class ServiceProvider.
 */
class ServiceProvider extends User
{
    protected $table = 'service_providers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'about_us',
        'logo',
        'currency_id',
        'address',
        'contact_number',
        'gst_number',
        'credentials',
        'tag_line',
        'status',
        'has_online_payment_support',
    ];

    /**
     * One to one relationship with service provider login
     */
    public function login()
    {
        return $this->hasOne(ServiceProviderLogin::class, 'service_provider_id');
    }

}
