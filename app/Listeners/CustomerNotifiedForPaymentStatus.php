<?php

namespace App\Listeners;

use App\Mail\PaymentStatus;
use App\Mail\PaymentInitiated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\NotifyCustomerForPaymentStatus;
use Twilio\Rest\Client;

class CustomerNotifiedForPaymentStatus implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NotifyCustomerForPaymentStatus  $event
     * @return void
     */
    public function handle(NotifyCustomerForPaymentStatus $event)
    {
        $payment = $event->payment;
        $customer = $payment->eCard;
        if (!empty($customer->linked_email)) {
            Mail::to($customer->linked_email)->send(new PaymentStatus($payment));
        }
        if (!empty($customer->linked_phone_number) && config('services.twilio.allow_to_send_sms')) {
            $msgBody = view('api.sms.payment.'.strtolower($payment->status), [
                'payment' => $payment
            ]);
            sendTwilioSMS($customer->linked_phone_number, $msgBody);
        }
    }
}
