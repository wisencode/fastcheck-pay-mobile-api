<?php

namespace App\Listeners;

use App\Mail\NotifyCustomerForRecharge;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerNotifiedForRecharge implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $transaction = $event->transaction;
        $customer = $transaction->eCard;
        if (!empty($customer->linked_email)) {
            Mail::to($customer->linked_email)->send(new NotifyCustomerForRecharge($transaction));
        }
        if (!empty($customer->linked_phone_number)) {
            $msgBody = view('api.sms.recharge.successful', [
                'transaction' => $transaction
            ]);
            sendTwilioSMS($customer->linked_phone_number, $msgBody);
        }
    }
}
