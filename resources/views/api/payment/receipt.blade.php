<html>
  <head>
    <title>Payment {{ $payment->transactionHash }}</title>
    <style type="text/css">
      @page {
        margin: 0px;
      }
      body {
        font-family: 'Segoe-UI', sans-serif;
        border-top: 2px dotted #01040c;
        background-color: #ffffff;
      }
      header div.banner-line {
        padding: 2px;
        text-align: center;
        color: #fff;
        background-color: #0c2536;
      }
      table.receipt-signature {
        width: 100%;
        margin-top: 5px;
      }
      table.receipt-signature .from-logo img {
        width: 60px;
        max-height: 60px;
        margin-left: 20px;
        border-radius: 50%;
        border: 1px solid silver;
        padding: 2px;
      }
      table.receipt-signature .app-logo img {
        margin-right: 20px;
        border: 1px solid silver;
        border-radius: 50%;
        padding: 2px;
      }
      table.receipt-signature .app-logo-mini img {
        max-width: 40px;
      }
      table.receipt.header.pending {
        border-bottom: 1px solid #24283045;
      }
      table.receipt.header.approved {
        border-bottom: 1px solid #24283045;
      }
      table.receipt.header.cancelled {
        border-bottom: 1px solid #24283045;
      }
      table.receipt {
        width: 100%;
        font-size: 12px;
        font-weight: bold;
        color: #242830;
      }
      table.receipt td.from table {
        width: 100%;
        text-align: left;
        margin-left: 20px;
      }
      table.receipt td.to table {
        width: 100%;
        text-align: right;
        margin-right: 20px;
      }
      .content {
        position: relative;
      }
      .content .trans-note {
        margin-left: 25px;
        margin-right: 10px;
        margin-top: 20px;
        margin-bottom: 30px;
      }
      table.payment {
        font-size: 16px;
        border-collapse: collapse;
        color: #242830;
        margin: 0 auto;
        border: 1px dashed #c9c9c9;
        border-top: 2px solid #000000;
        border-radius: 5px;
        background-color: #ececec;
      }
      table.payment.pending {
        border-top: 2px solid #3498db;
      }
      table.payment.approved {
        border-top: 2px solid #3CB371;
      }
      table.payment.cancelled {
        border-top: 2px solid #DC143C;
      }
      table.payment tr td, table.payment tr th {
        text-align: left;
      }
      table.payment tr:nth-child(even) {
        background-color: #F5F5F5;
      }
      table.payment td.status {
        font-weight: bold;
      }
      table.payment td.status.pending {
        color: #03a9f4;
      }
      table.payment td.status.approved {
        color: #3CB371;
      }
      table.payment td.status.cancelled {
        color: #DC143C;
      }
      .valign-top {
        vertical-align: top;
      }
      .w-50 {
        width: 50%;
      }
      .footer {
        color: grey;
        width: 100%;
        font-size: 16px;
        padding-top: 20px;
      }
      .footer a {
        text-decoration: underline;
        color: #000000;
      }
      .text-grey {
        color: silver;
      }
      .text-center {
        text-align: center;
      }
      .text-right {
        text-align: right;
      }
      footer div.banner-line {
        padding: 4px;
        text-align: center;
        background-color: #242830;
        color: #fff;
        font-size: 12px;
        border-bottom: 2px dotted #ededed;
      }
      footer div.banner-line a {
        color: #ededed;
      }
    </style>
  </head>
  <body>
    <header>
      <div class="banner-line {{ $payment->status }}">
        Fastcheck
      </div>

      <table class="receipt-signature">
        <tr>
          @if (!empty($payment->serviceProvider->logo))
            <td class="from-logo">
              <img src="{{ $payment->serviceProvider->logo_url }}" />
            </td>
          @endif      
          <td class="app-logo text-right">
            <img src="https://pay.fastcheck.io/assets/img/logo/60x60/bg-dark-circle.png" />
          </td>
        </tr>
      </table>
      
      <table class="receipt header {{ $payment->status }}">
        <tr>
          <td class="from valign-top w-50">
            <table>
              <tr>
                <td>{{ $payment->serviceProvider->name }}</td>
              </tr>
              <tr>
                <td>{{ $payment->serviceProvider->address }}</td>
              </tr>
            </table>
          </td>
          <td class="to valign-top w-50">
            <table>
              <tr>
                <td>{{ $payment->eCard->card_holder_name }}</td>
              </tr>
              <tr>
                <td>{{ $payment->created_date }}</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </header>
    
    <div class="content">
      <div class="trans-note">
          Hello <strong>{{ $payment->eCard->card_holder_name }}</strong>, find your transaction details at below:
      </div>
      <table class="payment {{ $payment->status }}" cellpadding="10" width="50%">
        <tbody>
        <tr>
          <td>Amount (Credits)</td>
          <td><strong>{{ number_format($payment->amount, 2) }}</strong></td>
        </tr>
        <tr>
          <td>FC Card #</td>
          <td><strong>{{ $payment->eCard->ecard_no }}</strong></td>
        </tr>
        <tr>
          <td>Transaction Hash</td>
          <td><strong>{{ $payment->transactionHash }}</strong></td>
        </tr>
        <tr>
          <td>Status</td>
          <td class="status {{ $payment->status }}">{{ ucfirst($payment->status) }}</td>
        </tr>
        <tr>
          <td>Remarks</td>
          <td class="{{ (empty($payment->remarks_note)) ? 'text-grey' : '' }}">{{ (!empty($payment->remarks_note)) ? $payment->remarks_note : 'No remarks' }}</td>
        </tr>
        </tbody>
      </table>

      <table class="receipt-signature footer">
        <tr>
          <td class="app-logo-mini text-center">
            <img src="https://pay.fastcheck.io/assets/img/logo/40x40/logo.png" />
          </td>
        </tr>
      </table>
    </div>

    <footer>
      <div class="banner-line {{ $payment->status }}">
        Report generated via <a href="{{ env('APP_VIA_WEBSITE') }}">{{ env('APP_VIA') }}</a>, Powered by <a href="{{ env('APP_POWERED_BY_WEBSITE') }}">{{ env('APP_POWERED_BY') }}</a>
      </div>
    </footer>

  </body>
</html>