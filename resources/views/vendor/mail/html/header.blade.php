<tr>
<td class="header" style="padding: 0px;padding-top:10px;">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="https://laravel.com/img/notification-logo.png" class="logo" alt="{{ config('app.via') }}">
@else
<img src="https://pay.fastcheck.io/assets/img/logo/60x60/bg-dark-circle.png" alt="{{ config('app.via') }}">
@endif
</a>
</td>
</tr>
<tr>
<td class="header" style="padding: 0px;padding-bottom:10px;">
{{ $slot }}
</td>
</tr>
