@component('mail::message')
# Hello {{ $payment->eCard->card_holder_name }},
 
Payment request of {{ $payment->amount }} credits is <span class="text-error"><strong><code>cancelled</code></strong></span> for <strong>#{{ $payment->eCard->ecard_no }}</strong>. Checkout the details below for the transaction ({{ $payment->transactionHash }})
 
@component('mail::table')
| Date          | Amount (Credits) | Current Balance (Credits) |
| ------------- | :--------------: | :-----------------------: |
| {{ $payment->created_date.' '.$payment->created_time }} | {{ $payment->amount }} | {{ number_format($payment->eCard->current_balance, 2) }}
@endcomponent

Thanks,<br>
{{ $payment->serviceProvider->name }}
@endcomponent