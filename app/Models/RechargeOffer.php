<?php

namespace App\Models;

use App\Domains\Auth\Models\Customer;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class RechargeOffer.
 */
class RechargeOffer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'eligible_amount',
        'service_provider_id',
        'offer_percentage',
        'description',
        'status',
        'created_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'eligible_amount' => 'float',
        'offer_percentage' => 'float'
    ];

    protected $appends = [
        'created_date',
        'created_time'
    ];

    public $timestamps = false;

    public function getCreatedDateAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d');
    }

    public function getCreatedTimeAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');
    }

    public function statusMode() {
        return ($this->status == 1) ? 'Active' : 'Inactive';
    }

    public function serviceProvider() {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }

}
