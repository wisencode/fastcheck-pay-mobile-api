<?php
namespace App\Http\Controllers\Api\V1\ServiceProvider;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;
use App\Models\ServiceProvider;
use App\Models\Currency;
use Carbon\Carbon;

class CurrencyController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $currencies = Currency::orderBy('name', 'ASC')->get();
            
            if(empty($currencies)) {
                return $this->respondWithError('No currency found!', 200);
            }
            
            return $this->respondWithSuccess('System currencies returned!', 200, $currencies);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

}