<?php
namespace App\Http\Controllers\Api\V1\ServiceProvider;

use Carbon\Carbon;
use App\Models\Currency;
use InterventionImageImage;
use Illuminate\Http\Request;
use App\Models\ServiceProvider;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Http\Controllers\Api\BaseApiController;

class ProfileController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $profile = ServiceProvider::find(auth()->user()->service_provider_id);
            $profile->eligible_for_gst = false;
            if ($profile->currency->id == 113) {
                $profile->eligible_for_gst = true;
            }
            
            if(empty($profile)) {
                return $this->respondWithError('No profile found!', 200);
            }
            
            $response['profile'] = $profile;
            return $this->respondWithSuccess('Service provider profile returned!', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    /**
     * @return json
     */
    public function update() {
        try {
            $rules = [
                'name' => ['required', 'max:100'],
                'about_us' => ['sometimes', 'max:180'],
                'address' => ['required', 'max:400'],
                'contact_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
                'gst_number' => ['sometimes'],
                'tag_line' => ['sometimes', 'max:180'],
                'has_online_payment_support' => ['required', 'integer', 'gte:0', 'lt:2'],
                'currency_id' => ['required', 'exists:currency,id']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'identifier.required' => 'identifier param is required',
                'has_online_payment_support.*' => 'has_online_payment_support param must be either 0 or 1'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $profile = ServiceProvider::find(auth()->user()->service_provider_id);
            $profile->name = $this->request->name ?? $profile->name;
            $profile->about_us = $this->request->about_us  ?? '';
            $profile->address = $this->request->address ?? $profile->address;
            $profile->contact_number = $this->request->contact_number ?? $profile->contact_number;
            $profile->gst_number = $this->request->gst_number ?? '';
            $profile->tag_line = $this->request->tag_line ?? '';
            $profile->has_online_payment_support = $this->request->has_online_payment_support ?? 0;
            $profile->currency_id = $this->request->currency_id ?? Currency::whereCode('INR')->first()->id;
            $profile->save();

            return $this->respondWithSuccess("Profile updated successfully", 200, ServiceProvider::find(auth()->user()->service_provider_id));

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }   
    }

    public function updateLogo() {
        try {
            $rules = [
                'logo' => ['required', 'mimes:jpg,jpeg,png,bmp,tiff', 'max:4096'],
            ];
            
            $validator = $this->validateParams($this->request->all(), $rules, [
                'logo.required' => 'logo is required param',
                'logo.mimes' => 'logo must be type of image file',
                'logo.max' => 'logo must be maximum of 4MB size'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            if ($logo = $this->request->file('logo')) {
                $uploadLogoAs = Carbon::now()->toDateString().'-'.uniqid().'.'.$logo->getClientOriginalExtension();
                $uploadLogoTo = 'assets/img/uploads/'.auth()->user()->service_provider_id.'/logo';
                $storage = Storage::disk('fcpay');

                foreach($storage->files($uploadLogoTo, true) as $oldLogo) {
                    $storage->delete($oldLogo);
                }

                if(!$storage->exists($uploadLogoTo)){
                    $storage->makeDirectory($uploadLogoTo);
                }
                
                $logo = Image::make($logo)->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->stream();
                $storage->put($uploadLogoTo.'/'.$uploadLogoAs, $logo);

                $serviceProvider = ServiceProvider::find(auth()->user()->service_provider_id);
                $serviceProvider->logo = $uploadLogoAs;
                $serviceProvider->save();

                return $this->respondWithSuccess("Logo successfully uploaded", 200);
            }

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    public function qr() {
        try {
            $format = env('QUICK_PAY_QR_FORMAT', 'svg');
            $myQuickPayQR = QrCode::size(150)
                            ->format($format)
                            ->color(56, 50, 62)
                            ->errorCorrection('H')
                            ->style('round')
                            ->generate(auth()->user()->service_provider_id);
            $uploadPayQRAs = 'service-provider-qpc.'.$format;
            $uploadPayQRTo = 'assets/img/uploads/'.auth()->user()->service_provider_id.'/quick-pay-code';
            Storage::disk('fcpay')->makeDirectory($uploadPayQRTo);
            Storage::disk('fcpay')->delete($uploadPayQRTo.'/'.$uploadPayQRAs);
            Storage::disk('fcpay')->put($uploadPayQRTo.'/'.$uploadPayQRAs, $myQuickPayQR);
            $serviceProvider = ServiceProvider::find(auth()->user()->service_provider_id);

            $myQuickPayQR = Image::make(Storage::disk('fcpay')->path($uploadPayQRTo.'/'.$uploadPayQRAs));
            $myQuickPayQR->insert(Storage::disk('fcpay')->path('assets/img/logo/mini-logo-white-bg.png'), 'bottom-right');
            $overlay = Image::canvas(150, 150);
            $overlay->text(
                strtoupper(substr($serviceProvider->name, 0, 20)),
                $myQuickPayQR->getWidth() / 2,
                0,
                function ($font) {
                    $font->color([255, 255, 255, 0.5]);
                    $font->size(48);
                    $font->align('center');
                    $font->valign('top');
                    $font->angle(10);
                });
            $myQuickPayQR->insert($overlay);
            $myQuickPayQR->save();

            $response = [
                'quick_pay_code' => $serviceProvider->myQuickPayCode
            ];

            return $this->respondWithSuccess("Quick Pay QRCode returned!", 200, $response);
            
        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

}