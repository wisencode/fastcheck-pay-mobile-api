<?php
namespace App\Http\Controllers\Api\V1\ServiceProvider;

use App\Events\NotifyCustomerForRecharge;
use App\Events\NotifyCustomersForRechargeOffer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\RechargeOffer;
use App\Mail\RechargeOfferAdded;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Api\BaseApiController;

class RechargeOfferController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function index()
    {
        try {
            $rechargeOffers = RechargeOffer::whereServiceProviderId(auth()->user()->service_provider_id)->whereStatus(1)->latest()->paginate(15);
            
            if(empty($rechargeOffers->items())) {
                return $this->respondWithError('No recharge offers found!', 200);
            }
            
            $response = [];
            foreach($rechargeOffers->items() as $index => $rechargeOffer) {
                $response[$index]['id'] = $rechargeOffer->id;
                $response[$index]['eligible_amount'] = $rechargeOffer->eligible_amount;
                $response[$index]['offer_percentage'] = $rechargeOffer->offer_percentage;
                $response[$index]['description'] = $rechargeOffer->description;
                $response[$index]['status'] = $rechargeOffer->statusMode();
                $response[$index]['created_date'] = $rechargeOffer->created_date;
                $response[$index]['created_time'] = $rechargeOffer->created_time;
            }
            
            $additionalData['pagination']['has_more_pages'] = $rechargeOffers->hasMorePages(); 
            
            return $this->respondWithSuccess('Recharge offers data returned!', 200, $response, $additionalData);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

    public function view() {
        try {
            $rules = [
                'recharge_offer_id' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'recharge_offer_id.required' => 'recharge_offer_id param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $rechargeOffer = RechargeOffer::find($this->request->recharge_offer_id);
            if (empty($rechargeOffer)) {
                return $this->respondWithError('Recharge offer not found', 404);
            }

            $response['recharge_offer'] = [
                'id' => $rechargeOffer->id,
                'eligible_amount' => $rechargeOffer->eligible_amount,
                'offer_percentage' => $rechargeOffer->offer_percentage,
                'description' => $rechargeOffer->description,
                'status' => $rechargeOffer->statusMode(),
                'created_date' => $rechargeOffer->created_date,
                'created_time' => $rechargeOffer->created_time
            ];
            return $this->respondWithSuccess('Recharge offer information returned', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);   
        }
    }

    public function create() {
        try {
            $rules = [
                'eligible_amount' => ['required', 'numeric', 'gt:0'],
                'offer_percentage' => ['sometimes', 'numeric', 'gte:0', 'lte:100'],
                'description' => ['sometimes', 'max:180']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'eligible_amount.required' => 'eligible_amount param is required',
                'offer_percentage.gte' => 'Offer percentage must be between 0 and 100',
                'offer_percentage.lte' => 'Offer percentage must be between 0 and 100'
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $rechargeOffer = new RechargeOffer();
            $rechargeOffer->eligible_amount = $this->request->eligible_amount;
            $rechargeOffer->offer_percentage = $this->request->offer_percentage ?? 0;
            $rechargeOffer->description = $this->request->description;
            $rechargeOffer->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $rechargeOffer->service_provider_id = auth()->user()->service_provider_id;
            $rechargeOffer->status = 1;
            $rechargeOffer->save();

            // Notify customers for new recharge offer
            event(new NotifyCustomersForRechargeOffer($rechargeOffer));

            return $this->respondWithSuccess('Recharge offer successfully added', 200, $rechargeOffer);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);   
        }
    }

    public function delete() {
        try {
            $rules = [
                'recharge_offer_id' => ['required']
            ];

            $validator = $this->validateParams($this->request->all(), $rules, [
                'recharge_offer_id.required' => 'recharge_offer_id param is required',
            ]);

            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $rechargeOffer = RechargeOffer::whereStatus(1)->whereId($this->request->recharge_offer_id)->first();
            if (empty($rechargeOffer)) {
                return $this->respondWithError('Recharge offer not found', 404);
            }

            $rechargeOffer->status = 0;
            $rechargeOffer->save();
            
            return $this->respondWithSuccess('Recharge offer successfully removed', 200);

        } catch (\Throwable $th) {

        }
    }

}