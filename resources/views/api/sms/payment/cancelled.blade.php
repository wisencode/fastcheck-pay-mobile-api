Hello {{ $payment->eCard->card_holder_name }},
Payment request of {{ $payment->amount }} credits is cancelled for card #{{ $payment->eCard->ecard_no }}.
Amount: {{ $payment->amount }} credits
Current Balance: {{ number_format($payment->eCard->current_balance, 2) }} credits
Date: {{ $payment->created_date }}
Hash: {{ $payment->transactionHash }}

Thanks,
{{ $payment->serviceProvider->name }}
