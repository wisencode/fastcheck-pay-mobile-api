@component('mail::message')
# Hello {{ $transaction->eCard->card_holder_name }},
 
Recharge of {{ number_format($transaction->actual_amount, 2) }} has been succesfully done for FC-ECard <strong>#{{ $transaction->eCard->ecard_no }}</strong>. <strong><span style="color: darkslategrey;font-weight: 500;">{{ number_format($transaction->transaction_amount, 2) }} credits</span></strong> have been added to your balance.</strong>

<p style="margin: 0px;margin-top: 8px;font-size: 12px; font-weight: bold;text-align: center;color: #e56d65;">Balance</p>
<p style="margin: 0px;margin-top: 1px;margin-bottom: 10px;text-align: center;color: darkslategrey;"><span style="font-size: 18px;font-weight: bold;">{{ number_format($transaction->eCard->current_balance, 2) }}</span><span style="font-size: 12px; font-weight: bold;text-align: center;"> credits</span></p>

@if(!empty($transaction->offer_percentage) && $transaction->offer_percentage > 0)
<p style="text-align: center;font-size: 13px;font-weight: 500;color: #2f6c0f;margin-bottom: 10px;line-height: normal;" class="text-success">
Congratulations! You received extra {{ number_format($transaction->transaction_amount - $transaction->actual_amount, 2) }} ({{ $transaction->offer_percentage }}%) credits with this recharge.
</p>
@endif

<p style="padding-top: 10px;">
Thanks,<br>
{{ $transaction->serviceProvider->name }}
</p>
@endcomponent