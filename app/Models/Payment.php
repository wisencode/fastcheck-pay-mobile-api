<?php

namespace App\Models;

use App\Domains\Auth\Models\Customer;
use App\Models\ServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;

/**
 * Class Payment.
 */
class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transactionHash',
        'service_provider_id',
        'amount',
        'remarks_note',
        'status',
        'customer_name',
        'fc_ecard_id',
        'created_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'float',
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public $timestamps = false;

    public function getCreatedDateAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d');
    }
    
    public function getCreatedTimeAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');
    }

    public function eCard()
    {
        return $this->belongsTo(Customer::class, 'fc_ecard_id');
    }

    public function serviceProvider() {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }

    public function getReceiptURLAttribute() {
        $pdf = PDF::loadView('api.payment.receipt', [
            'payment' => $this
        ]);
        $receiptFileUploadTo = 'payment/receipts/';
        $receiptFileSaveAs = 'FCPay-Rcpt-'.$this->id.$this->transactionHash.'.pdf';
        \Storage::disk('public')->put($receiptFileUploadTo.$receiptFileSaveAs, $pdf->output(), 'public');
        return \Storage::disk('public')->url($receiptFileUploadTo.$receiptFileSaveAs);
    }

}
