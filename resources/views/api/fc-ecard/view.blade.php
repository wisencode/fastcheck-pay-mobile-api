<html>
  <head>
    <title>FC-ECard {{ $card->ecard_no }}</title>
    <style type="text/css">

      body {
        margin: 0 auto;
        padding: 0;
        background: #e2e1e160;
        border-bottom: 10px solid #242830;
        font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
      }

      .left {
        left: 25px;
      }

      .right {
        right: 25px;
      }

      .center {
        text-align: center;
      }

      .bottom {
        position: absolute;
        bottom: 25px;
      }

      #gradient {
        background: #21BF73;
        margin: 0 auto;
        margin-top: 100px;
        width: 100%;
        height: 10px;
      }

      #gradient:after {
        content: "";
        position: absolute;
        background: #242830;
        left: 50%;
        margin-top: -67.5px;
        margin-left: -270px;
        padding-left: 20px;
        border-radius: 5px;
        width: 520px;
        height: 275px;
        z-index: -1;
      }

      #card {
        position: absolute;
        height: 225px;
        min-width: 250px;
        padding: 25px;
        padding-top: 0;
        padding-bottom: 0;
        left: 47%;
        top: 67.5px;
        margin-left: -250px;
        z-index: 5;
        color: #ffffff;
      }

      #card img.fc_logo {
        position: fixed;
        top: 20px;
        left: 0px;
        float: left;
        height: 20px;
        width: 20px;
      }
      #card p.app-title {
        position: absolute;
        top: 20px;
        right: 0px;
        margin-top: -45px;
        float: right;
      }

      #card img {
        width: 150px;
        float: left;
        border-radius: 5px;
        margin-right: 20px;
        margin-top: 20px;
        -webkit-filter: sepia(1);
        -moz-filter: sepia(1);
        filter: sepia(1);
      }

      #card h2 {
        color: #efefef;
        margin: 0 auto;
        padding: 0;
        font-size: 15pt;
      }

      #card h4 {
        color: #efefef;
        margin: 0 auto;
        padding: 0;
        margin-bottom: 10px;
        margin-top: 20px;
      }

      #card p {
        color: #dedede;
        font-size: 13px;
      }
      #card .text-sm {
        font-size: small;
      }

      #card .card-holder-name {
        letter-spacing: 3px;
      }

      #card .card-eligible-note {
        width: 500px;
      }

      .footer {
        float: none;
        margin: 0 auto;
        text-align: center;
        margin-top: 230px;
        color: #24283080;
        width: 100%;
      }
      .footer .logo {
        margin-top: 30px;
        height: 45px;
        width: 45px;
      }
      .footer a {
        margin-top: 5px;
        color: #242830;
        text-decoration: none;
      }
      .footer .d-block {
        display: block;
      }
      .footer .mpb-2p {
        margin: 0px;
        padding: 0px;
        padding-bottom: 2px !important;
        margin-bottom: 2px !important;
      }

      .text-bold {
        font-weight: bold;
      }

    </style>
  </head>
  <body>
    <div id="gradient"></div>
    <div id="card">
      <img src="https://pay.fastcheck.io/assets/img/logo/x-small/fc_logo.png" class="fc_logo" />
      <p class="app-title">Fastcheck</p>
      <img src="data:image/png;base64,{{ $cardQR }}" />
      <h4>{{ $card->serviceProvider->name }}</h4>
      <h2 class="card-holder-name">{{ $card->display_name }}</h2>
      <p>{{ $card->card_holder_name }}</p>
      <p class="card-eligible-note">This card is only valid for {{ $card->serviceProvider->name }}</p>
      <span class="left bottom text-sm">Powered by WisenCode Infotech</span>
      <span class="right bottom">{{ $card->ecard_no }}</span>
    </div>
    <div class="footer">
      @if (!empty($card->serviceProvider->name))
      <p class="d-block mpb-2p text-bold">{{ $card->serviceProvider->name }}</p>
      @endif
      @if (!empty($card->serviceProvider->address))
      <p class="d-block mpb-2p">{{ $card->serviceProvider->address }}</p>
      @endif
      @if (!empty($card->serviceProvider->contact_number))
      <p class="d-block mpb-2p">{{ $card->serviceProvider->contact_number }}</p>
      @endif
      <img src="https://pay.fastcheck.io/assets/img/logo/60x60/bg-dark-circle.png" class="logo d-block" />
      <p class="d-block">
        Generated via <a href="{{ config('app.via_website') }}">{{ config('app.via') }}</a>, Powered by <a href="{{ config('app.powered_by_website') }}">{{ config('app.powered_by') }}</a>
      </p>
    </div>
  </body>
</html>