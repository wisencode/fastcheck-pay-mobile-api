<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

/**
 * Class BaseApicontroller
 * @package App\Http\Controllers\API
 */
class BaseApiController extends Controller
{
	const FAILURE = 0;
    const SUCCESS = 1;
    const SESSION_EXPIRED = -1;

    /**
     * @param  string      $message
     * @param  int|integer $httpCode
     * @param  array       $data
     * @param  array       $additionalData
     * @return JSON
     */
    protected function respondWithSuccess($message = "Success!", $httpCode = 200, $data = null, $additionalData = null)
    {   
        $response['success'] = 1;
        $response['message'] = $message;
        $response['data'] = $data;

        $additionalData['unread_notification_count'] = $this->unreadNotificationCount();
        $response['additional_data'] = $additionalData;
        
        // $data = $this->replaceNullWithEmptyString($data);
        return response()->json($response, $httpCode);
    }
    
    /**
     * @param  string      $message
     * @param  int|integer $httpCode
     * @param  int         $errorCode
     * @param  array       $data
     * @param  array       $additionalData
     * @return JSON
     */
    protected function respondWithError($message = "Error!", $httpCode = 500, $errorCode = null, $data = null, $additionalData = null)
    {
        $response['success'] = 0;
        $response['message'] = $message;
        $response['error_code'] = $errorCode ?? $httpCode;
        $response['data'] = $data;

        $additionalData['unread_notification_count'] = $this->unreadNotificationCount();
        $response['additional_data'] = $additionalData;
        
        // $data = $this->replaceNullWithEmptyString($data);
        return response()->json($response, $httpCode);
    }

    /**
     * [validateParams is a custom validator function]
     * @param  [array] $data [validation params]
     * @return [object]      [Success or Failure object]
     */
    public function validateParams($data, $rules, $messages)
    {
        return Validator::make($data, $rules, $messages);
    }

    /**
     * @return int
     */
    public function unreadNotificationCount(): int
    {
        if(auth()->check()){
            return 0;
            // return auth()->user()->notifications()->where('is_read', 0)->count();
        }
        
        return 0;
    }

    protected function replaceNullWithEmptyString($array)
    {
        if(is_null($array)){
            return $array;
        }

        if(!is_array($array)) {
            $array = $array->toArray();
        }
        
        foreach ($array as $key => $value) 
        {
            if(is_array($value))
                $array[$key] = $this->replaceNullWithEmptyString($value);
            else
            {
                if (is_null($value))
                    $array[$key] = "";
            }
        }
        return $array;
    }
}
