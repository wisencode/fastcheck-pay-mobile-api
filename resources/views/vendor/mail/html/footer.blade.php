<tr>
<td>
<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
@if(empty($isPromotional))
<tr>
<td class="content-cell" align="center" style="padding-top: 10px; padding-bottom: 0px;">
    <p style="font-size: 12px;color: #413f4e;">
        <i>You are receiving this email because your email address is linked with our system. Kindly contact us if you have any concern associated with this email.</i>
    </p>
</td>
</tr>
@endif
<tr>
<td class="content-cell" align="center" style="padding-top: 0px;">
<img src="https://pay.fastcheck.io/assets/img/logo/40x40/logo.png" alt="{{ config('app.via') }}">
<div align="center">
Mail sent via <a href="{{ config('app.via_website') }}">{{ config('app.via') }}</a>, Powered by <a href="{{ config('app.powered_by_website') }}">{{ config('app.powered_by') }}</a>
</div>
</td>
</tr>
</table>
</td>
</tr>
