@component('mail::message')
# Hello Fastcheck Payment Users,
 
New Recharge Offer found for your FC-ECards. Please checkout below for more information about this offer

<p style="margin: 0px;margin-top: 8px;font-size: 18px; font-weight: bold;text-align: center;color: #009688;">Get {{ $rechargeOffer->offer_percentage }}% extra credits on recharge of {{ $rechargeOffer->eligible_amount }}</p>

@if (!empty($rechargeOffer->description))
<p style="text-align: center;font-size: 13px;font-weight: 500;color: #607d8b;margin-bottom: 10px;line-height: normal;" class="text-success">
{{ $rechargeOffer->description }}
</p>
@endif

<p style="padding-top: 10px;">
Thanks,<br>
{{ $rechargeOffer->serviceProvider->name }}
</p>
@endcomponent