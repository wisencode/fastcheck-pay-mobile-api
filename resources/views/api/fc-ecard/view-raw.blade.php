@component('mail::message')
# Hello {{ $card->card_holder_name }},

<p style="font-size: 14px;">
We are sending you a digital copy of your FC ECard <strong><code>{{ $card->ecard_no }}</code></strong> along with this email. Please check out the attached PDF file for the digital version of your FC ECard. This digital copy can be used as an actual card for the payment/transaction at {{ $card->serviceProvider->name }}, {{ $card->serviceProvider->address }}.
</p>

<p style="font-size: 13px;text-align: center;font-weight: bold;padding-top: 10px;">
<span style="color:rgb(71, 168, 104);padding-bottom: 3px;"><strong>Thank you for being a customer</strong></span><br>
<span style="color: #252020;">{{ $card->serviceProvider->name }}</span><br>
</p>

@endcomponent