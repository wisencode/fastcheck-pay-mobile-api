<?php

use Carbon\Carbon;
use Twilio\Rest\Client;

if (! function_exists('appName')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function appName()
    {
        return config('app.name', 'Laravel Boilerplate');
    }
}

if (! function_exists('carbon')) {
    /**
     * Create a new Carbon instance from a time.
     *
     * @param $time
     *
     * @return Carbon
     * @throws Exception
     */
    function carbon($time)
    {
        return new Carbon($time);
    }
}

if (! function_exists('homeRoute')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function homeRoute()
    {
        if (auth()->check()) {
            if (auth()->user()->isAdmin()) {
                return 'admin.dashboard';
            }

            if (auth()->user()->isUser()) {
                return 'frontend.user.dashboard';
            }
        }

        return 'frontend.index';
    }
}

if (! function_exists('generateOTP')) {
    /**
     * generate an OTP
     * @param int $length
     * @return string
     */
    function generateOTP($length = 4)
    {
        if(1 == 2 && app()->environment(['local', 'testing'])) {
            return implode('', range(1, $length));
        }
        else {
            return sprintf('%0'.$length.'d', mt_rand(1, str_repeat('9', $length)));
        }
    }
}

if (! function_exists('generateHash')) {
    /**
     * generate an hash
     * @param int $length
     * @return string
     */
    function generateHash($length = 16)
    {
        $chunksToShuffle = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $hash = '';
        for ($start = 0; $start < $length; $start++) {
            $index = rand(0, strlen($chunksToShuffle) - 1);
            $hash.= $chunksToShuffle[$index];
        }
        return $hash.'_'.uniqid();
    }
}

if (! function_exists('generateFCECardNumber')) {
    /**
     * generate an FC-ECard Number
     * @param int $fc_eCardId
     * @param int $service_provider_id
     * @return string
     */
    function generateFCECardNumber($fc_eCardId, $service_provider_id)
    {
        $length = 5;
        $FCECardNumber = 'FC';
        $chunks = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
            $FCECardNumber.= strtoupper($chunks[array_rand($chunks)]);
        }
        $FCECardNumber.= str_pad($fc_eCardId, 4, '0', STR_PAD_LEFT);
        $FCECardNumber.= 'P'.$service_provider_id;
        return $FCECardNumber;
    }
}

if (! function_exists('encryptData')) {
    /**
     * Encrypt a string using salt
     */
    function encryptData($data = '') {
        if (empty($data)) {
            return '';
        }
        $result = '';
        for ($i = 0, $k = strlen($data); $i<$k; $i++) {
            $char = substr($data, $i, 1);
            $keychar = substr(config('app.encryption_salt'), ($i % strlen(config('app.encryption_salt')))-1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result.= $char;
        }
        return base64_encode($result);
    }
}

if (! function_exists('decryptData')) {
    /**
     * Decrypt a string using salt
     */
    function decryptData($data = '') {
        $result = '';
        $data = base64_decode($data);
        for($i = 0,$k = strlen($data); $i < $k ; $i++) {
            $char = substr($data, $i, 1);
            $keychar = substr(config('app.encryption_salt'), ($i % strlen(config('app.encryption_salt'))) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result.= $char;
        }
        return $result;
    }
}

if (! function_exists('getRandomColor')) {
    /**
     * Get a random color
     * @param int position
     * @param bool $dark
     */
    function getRandomColor($index = 0, $dark = true) {
        $darkColors = [
            '033069', '7e00ff', '3F3A35', 'FFCE47', 'Af572f', 'AF2F4E', '2FAF45', '525252', '4A3030', '303E4A', 'C9c966', '3F6D61', '1E4E41', '148364', '717171', '000000', '52377d', '564b66', '4b4e66', '44444e', '41102e', '624A59', '582840', '0074ff', 'Ff6400', 'Ff9000', 'Ff3e00', 'Ff0062', '4A4775', 'AD315F', '6C6C6C', '947F91', 'CD8E9D', 'FDA38F', 'FFC777', '9F9F9F', '00C896', '474554', '05C19C', '868395', '474554', 'FF6B1C', 'FF825C', '0098C4', '69BAA5', '797585', '517C96', '827E49', '334B4A'
        ];
        if ($index > (count($darkColors) -1)) {
            $index = 0;
        } 
        return $darkColors[$index];
    }
}

if (! function_exists('sendTwilioSMS')) {
    /**
     * Send a sms via twilio
     * @param int position
     * @param bool $dark
     */
    function sendTwilioSMS($to, $msgBody, $addSignature = true) {
        try {
            if ($addSignature)
                $msgBody .= 'SMS via '.config('app.via').', Powered by '.config('app.powered_by');
            $client = new Client(config('services.twilio.account_sid'), config('services.twilio.auth_token'));
            $client->messages->create($to, [
                'from' => config('services.twilio.phone_number'),
                'body' => $msgBody
            ]);
        } catch (\Throwable $th) {
            // Ignore background errors for a while
        }
    }
}

