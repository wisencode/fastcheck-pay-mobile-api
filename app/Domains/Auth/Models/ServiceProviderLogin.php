<?php

namespace App\Domains\Auth\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User;

/**
 * Class ServiceProviderLogin.
 */
class ServiceProviderLogin extends User
{
    use HasApiTokens;

    protected $table = 'service_provider_login';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_provider_id',
        'username',
        'password',
        'role',
        'status',
    ];

    /**
     * One to one relationship with service provider
     */
    public function serviceProvider()
    {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }
}
