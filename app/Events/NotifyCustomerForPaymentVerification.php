<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotifyCustomerForPaymentVerification
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $payment;
    public $payment_verification_code;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($payment, $payment_verification_code)
    {
        $this->payment = $payment;
        $this->payment_verification_code = $payment_verification_code;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [];
    }
}
