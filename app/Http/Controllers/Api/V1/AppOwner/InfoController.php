<?php
namespace App\Http\Controllers\Api\V1\AppOwner;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseApiController;

class InfoController extends BaseApiController
{
    /**
     * Request
     */
    protected Request $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return json
     */
    public function contact()
    {
        try {
            $response = [
                'name' => config('app.powered_by'),
                'email_address' => config('app.powered_by_email_address'),
                'contact_number' => config('app.powered_by_contact_number')
            ];
            return $this->respondWithSuccess("Owner contact info returned!", 200, $response);
        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), !empty($th->getCode())? $th->getCode() : 500);
        }
    }

}