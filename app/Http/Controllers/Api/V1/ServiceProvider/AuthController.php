<?php

namespace App\Http\Controllers\Api\V1\ServiceProvider;

use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use App\Domains\Auth\Models\Customer;
use App\Domains\Auth\Models\ServiceProviderLogin;
use App\Http\Controllers\Api\BaseApiController;
use Illuminate\Auth\Access\AuthorizationException;

class AuthController extends BaseApiController
{
    /**
     * Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * @return json
     */
    public function login()
    {
        try {
            $rules = [
                'username' => ['required'],
                'password' => ['required'],
            ];

            $validator = $this->validateParams($this->request->all(), $rules, []);
    
            if ($validator->fails()) {
                return $this->respondWithError($validator->errors()->first(), 400);
            }

            $serviceProviderLogin = ServiceProviderLogin::whereUsername($this->request->username)->wherePassword(encryptData($this->request->password))->whereStatus(1)->first();
            
            if (! $serviceProviderLogin) {    
                throw new GeneralException('Account does not exists with this username/password.', 400);
            }

            $response = $this->generateLoginResponse($serviceProviderLogin);
            return $this->respondWithSuccess('Logged in successfully!', 200, $response);

        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }
    
    /**
     * Resend otp for verification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     * 
     * @throws AuthorizationException
     */
    public function resendOTP()
    {
        try {
            $token = $this->request->verification_token;
            if(!$token) {
                throw new AuthorizationException;
            }

            $token = $this->request->verification_token;
            $id = explode('-', $token)[0];
            $cardNo = explode('-', $token)[1];
            
            $customer = Customer::where('id', $id)->first();
            
            if(!$customer || ($customer && md5($customer->ecard_no) != $cardNo)) {
                throw new GeneralException('Invalid verification token!', 401);
            }

            $customer->verificationCode()->updateOrCreate([
                'verification_code' => generateOTP(),
            ]);

            $customer->sendPhoneVerificationNotification();
            $customer->sendEmailVerificationNotification();

            return $this->respondWithSuccess('OTP resent successfully!', 200);
        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }
    
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     * 
     * @throws AuthorizationException
     */
    public function verifyOtp()
    {
        try {
            $token = $this->request->verification_token;
            if(!$token) {
                throw new AuthorizationException;
            }
            
            $validator = $this->validateParams($this->request->all(), [
                'otp' => 'required',
                'device_type' => ['sometimes', 'nullable', 'in:android,ios,web'],
                'device_token' => ['sometimes', 'nullable', 'string'],
            ], []);
            
            if($validator->fails()) {
                throw new GeneralException($validator->errors()->first(), 400);
            }

            $token = $this->request->verification_token;
            $id = explode('-', $token)[0];
            $cardNo = explode('-', $token)[1];
            
            $customer = Customer::where('id', $id)->first();
            
            if(!$customer || ($customer && md5($customer->ecard_no) != $cardNo)) {
                throw new GeneralException('Invalid verification token!', 401);
            }

            if ($customer->verificationCode->verification_code != $this->request->otp) {
                throw new AuthorizationException('Invalid OTP!', 401);
            }
            
            // $customer->device_type = $this->request->device_type;
            // $customer->device_token = $this->request->device_token;
            // $customer->save();
                
            $response = $this->generateLoginResponse($customer);
            return $this->respondWithSuccess('Logged in successfully!', 200, $response);
        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }

    /**
     * Logout a user
     */
    public function logout()
    {
        try {
            if(!$this->request->user()->currentAccessToken()->delete()) {
                throw new GeneralException('Unable to delete token! Try again later!', 422);
            }
            
            return $this->respondWithSuccess('You have been logged out successfully!', 200);
            
        } catch (\Throwable $th) {
            return $this->respondWithError($th->getMessage(), (!empty($th->getCode())? $th->getCode() : 500));
        }
    }

    /**
     * @param ServiceProviderLogin $serviceProviderLogin
     * @param bool $generateToken
     * 
     * @return Array
     */
    protected function generateLoginResponse(ServiceProviderLogin $serviceProviderLogin, $generateToken = true) : array
    {
        $response = [];

        if($serviceProviderLogin){
            $response['name'] = optional($serviceProviderLogin->serviceProvider)->name;
            $response['about_us'] = optional($serviceProviderLogin->serviceProvider)->about_us;
            $response['address'] = optional($serviceProviderLogin->serviceProvider)->address;
            $response['contact_number'] = optional($serviceProviderLogin->serviceProvider)->contact_number;
            $response['gst_number'] = optional($serviceProviderLogin->serviceProvider)->gst_number;
            $response['tag_line'] = optional($serviceProviderLogin->serviceProvider)->tag_line;
            $response['has_online_payment_support'] = optional($serviceProviderLogin->serviceProvider)->has_online_payment_support;
            if($generateToken) {
                $response['token'] = $serviceProviderLogin->createToken('sp-token')->plainTextToken;
            }
            else{
                $response['token'] = request()->bearerToken();
            }
        }
        return $response;
    }
}
