<?php

namespace App\Models;

use App\Domains\Auth\Models\Customer;
use Illuminate\Database\Eloquent\Model;
use App\Models\RechargeOffer;
use Carbon\Carbon;

/**
 * Class OnlinePaymentLog.
 */
class OnlinePaymentLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'service_provider_id',
        'payment_for',
        'recharge_offer_id',
        'amount',
        'description',
        'pay_with',
        'status',
        'fc_ecard_id',
        'created_at'
    ];

    protected $table = 'online_payments_log';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'float',
    ];

    public function getCreatedDateAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d');
    }
    public function getCreatedTimeAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');
    }

    public function getDescriptionAttribute($description) 
    {
        // if (!empty($description)) {
        //     return unserialize($description);
        // }
        return $description;
    }

    public function eCard()
    {
        return $this->belongsTo(Customer::class, 'fc_ecard_id');
    }

    public function offer()
    {
        return $this->belongsTo(RechargeOffer::class, 'recharge_offer_id');
    }

}
