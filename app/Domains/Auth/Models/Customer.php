<?php

namespace App\Domains\Auth\Models;

use App\Models\Payment;
use App\Models\OnlinePaymentLog;
use App\Models\CustomerTransactionLog;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User;
use Carbon\Carbon;

/**
 * Class Customer.
 */
class Customer extends User
{
    use HasApiTokens;

    protected $table = 'fc_ecards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ecard_no',
        'service_provider_id',
        'current_balance',
        'display_name',
        'linked_phone_number',
        'linked_email',
        'status',
        'is_locked',
        'has_payment_verification',
        'valid_for',
        'card_holder_name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_locked' => 'boolean',
        'has_payment_verification' => 'boolean',
    ];

    public function getCreatedDateAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d');
    }

    public function getCreatedTimeAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');
    }

    public function transactionsLog() {
        return $this->hasMany(CustomerTransactionLog::class, 'fc_ecard_id', 'id')->orderBy('created_at', 'DESC');
    }
    public function payments() 
    {
        return $this->hasMany(Payment::class, 'fc_ecard_id', 'id');
    }
    public function recharges() 
    {
        return $this->hasMany(OnlinePaymentLog::class, 'fc_ecard_id', 'id')->where('payment_for', 'ONLINE_RECHARGE');
    }

    public function getNameInitialsAttribute() {
        if (!empty($this->card_holder_name)) {
            if (strlen($this->card_holder_name) == 1) {
                return strtoupper($this->card_holder_name);
            }
            $card_holder_name_parts = explode(' ', trim($this->card_holder_name));
            if (count($card_holder_name_parts) == 1) {
                return strtoupper(mb_substr(trim($card_holder_name_parts[0]), 0, 1)).strtoupper(mb_substr(trim($card_holder_name_parts[0]), 1, 1));
            } else {
                return strtoupper(mb_substr(trim($card_holder_name_parts[0]), 0, 1)).strtoupper(mb_substr(trim($card_holder_name_parts[count($card_holder_name_parts) - 1]), 0, 1));
            }
        }
        return 'NA';
    }

    /**
     * Send the registration verification email.
     */
    public function sendEmailVerificationNotification(): void
    {   
        // $this->notify(new VerifyEmail);
    }

    /**
     * Send the phone verification notification.
     *
     * @return void
     */
    public function sendPhoneVerificationNotification()
    {
        // 
    }

    public function verificationCode() {
        return $this->hasOne(VerificationCode::class, 'fc_ecard_id');
    }

    public function serviceProvider() {
        return $this->belongsTo(ServiceProvider::class, 'service_provider_id');
    }
}
