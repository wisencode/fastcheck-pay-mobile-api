<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RechargeOfferAdded extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $rechargeOffer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($rechargeOffer)
    {
        $this->rechargeOffer = $rechargeOffer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                ->subject('New recharge offer arrived for FC-ECard - '.$this->rechargeOffer->serviceProvider->name)
                ->markdown('api.mail.recharge-offer.added', [
                    'rechargeOffer' => $this->rechargeOffer
                ]);
    }
}
