<?php

namespace App\Listeners;

use App\Mail\PaymentInitiated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\NotifyCustomerForPaymentVerification;
use Twilio\Rest\Client;

class CustomerNotifiedForPaymentVerification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NotifyCustomerForPaymentVerification  $event
     * @return void
     */
    public function handle(NotifyCustomerForPaymentVerification $event)
    {
        $payment = $event->payment;
        $customer = $payment->eCard;
        $payment_verification_code = $event->payment_verification_code;
        $payLink = config('app.fcpay_root_url').'services/pay-with-fc-card.php?token=';
        $fcECardPaymentHashWith[] = $payment->serviceProvider->id;
        $fcECardPaymentHashWith[] = $payment->id;
        $fcECardPaymentHashWith[] = $payment->eCard->ecard_no;
        $fcECardPaymentHashWith[] = 'fcp';
        $token = encryptData(implode('||', $fcECardPaymentHashWith));
        $payLink .= urlencode($token);
        $transaction = [
            'payLink' => $payLink,
            'payment' => $payment,
            'payment_verification_code' => $payment_verification_code
        ];
        if (!empty($customer->linked_email)) {
            Mail::to($customer->linked_email)->send(new PaymentInitiated((object) $transaction));
        }
        if (!empty($customer->linked_phone_number) && config('services.twilio.allow_to_send_sms')) {
            $msgBody = view('api.sms.payment.initiated', [
                'payLink' => $payLink,
                'payment' => $payment,
                'payment_verification_code' => $payment_verification_code,
                'sendPayLink' => true
            ]);
            sendTwilioSMS($customer->linked_phone_number, $msgBody, false);
        }
    }
}
