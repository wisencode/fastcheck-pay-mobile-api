Hello {{ $payment->eCard->card_holder_name }},

OTP for {{ $payment->amount }} credits on card #{{ $payment->eCard->ecard_no }} is {{ $payment_verification_code }}
@if($sendPayLink)
Click this link {{ $payLink }} to verify your payment directly from your end
@endif

Thanks,
{{ $payment->serviceProvider->name }}
