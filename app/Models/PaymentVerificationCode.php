<?php

namespace App\Models;

use App\Domains\Auth\Models\Payment;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class PaymentVerificationCode.
 */
class PaymentVerificationCode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_provider_id',
        'payment_id',
        'verification_code',
        'created_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public $timestamps = false;

    public function getCreatedDateAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d');
    }
    
    public function getCreatedTimeAttribute() 
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('H:i:s');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

}
